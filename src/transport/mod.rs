pub mod articles;
pub mod comments;

use crate::core::traits::*;

pub struct Transport<T: Repositories> {
    repository: T,
}

pub fn new<T: Repositories>(repository: T) -> Transport<T> {
    Transport { repository }
}
