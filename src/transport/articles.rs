use crate::core::{
    models::*,
    traits::{transport::Articles, Repositories},
};
use axum::async_trait;

#[async_trait]
impl<T: Repositories + std::marker::Sync + std::marker::Send> Articles for super::Transport<T> {
    async fn create_article(&self, article: Article) -> Result<String, Error> {
        from_result::<String>(self.repository.create_article(article).await)
    }
    async fn update_article_text(&self, id: String, text: String) -> Option<Error> {
        from_option(self.repository.update_article_text(id, text).await)
    }
    async fn update_article_title(&self, id: String, title: String) -> Option<Error> {
        from_option(self.repository.update_article_title(id, title).await)
    }
    async fn get_article_by_title(&self, title: String) -> Result<String, Error> {
        from_result::<String>(self.repository.get_article_by_title(title).await)
    }
    async fn get_article_id(&self, name: String, password: String) -> Result<String, Error> {
        from_result::<String>(self.repository.get_article_id(name, password).await)
    }
    async fn delete_article(&self, id: String) {
        self.repository.delete_article(id).await
    }
}
