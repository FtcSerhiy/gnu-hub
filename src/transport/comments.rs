use crate::core::{
    models::*,
    traits::{transport::Comments, Repositories},
};
use axum::async_trait;

#[async_trait]
impl<T: Repositories + std::marker::Sync + std::marker::Send> Comments for super::Transport<T> {
    async fn add_comment(&self, title: String, content: String) -> Option<Error> {
        from_option(self.repository.add_comment(title, content).await)
    }
    async fn get_all_comments(&self, title: String) -> Result<Vec<String>, Error> {
        from_result::<Vec<String>>(self.repository.get_all_comments(title).await)
    }
}
