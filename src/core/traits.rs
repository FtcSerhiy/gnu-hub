use axum::async_trait;

#[async_trait]
pub trait Repositories: repository::Articles + repository::Comments {
    async fn new() -> Self;
}

#[async_trait]
pub trait Transports: transport::Articles + transport::Comments {}

pub mod repository {
    use crate::core::models::Article;
    use axum::async_trait;
    use sqlx::Error;

    #[async_trait]
    pub trait Articles {
        async fn create_article(&self, article: Article) -> Result<String, Error>;
        async fn update_article_title(&self, id: String, title: String) -> Option<Error>;
        async fn update_article_text(&self, id: String, text: String) -> Option<Error>;
        async fn get_article_id(&self, title: String, password: String) -> Result<String, Error>;
        async fn get_article_by_title(&self, title: String) -> Result<String, Error>;
        async fn delete_article(&self, id: String);
    }
    #[async_trait]
    pub trait Comments {
        async fn add_comment(&self, title: String, content: String) -> Option<Error>;
        async fn get_all_comments(&self, title: String) -> Result<Vec<String>, Error>;
    }
}

pub mod transport {
    use crate::core::models::{Article, Error};
    use axum::async_trait;

    #[async_trait]
    pub trait Articles {
        async fn create_article(&self, article: Article) -> Result<String, Error>;
        async fn update_article_title(&self, id: String, title: String) -> Option<Error>;
        async fn update_article_text(&self, id: String, text: String) -> Option<Error>;
        async fn get_article_id(&self, title: String, password: String) -> Result<String, Error>;
        async fn get_article_by_title(&self, title: String) -> Result<String, Error>;
        async fn delete_article(&self, id: String);
    }
    #[async_trait]
    pub trait Comments {
        async fn add_comment(&self, title: String, content: String) -> Option<Error>;
        async fn get_all_comments(&self, title: String) -> Result<Vec<String>, Error>;
    }
}
