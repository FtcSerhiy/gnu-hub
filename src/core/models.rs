use axum::Json;
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};

#[derive(Serialize, Deserialize)]
pub struct Article {
    pub title: String,
    pub password: String,
    pub content: String,
}

#[derive(Serialize, Deserialize)]
pub struct Comment {
    pub title: String,
    pub content: String,
}

#[derive(Serialize, Deserialize)]
pub struct Auth {
    pub name: String,
    pub password: String,
}

#[derive(Serialize, Deserialize)]
pub enum Error {
    BadClientData,
    InternalError,
}

#[derive(sqlx::FromRow)]
pub struct Str {
    pub content: String,
}

impl Str {
    pub fn map(result: Result<Vec<Str>, sqlx::Error>) -> Result<Vec<String>, sqlx::Error> {
        match result {
            Err(err) => Err(err),
            Ok(vec) => Str::transform(vec),
        }
    }
    fn transform(vec: Vec<Str>) -> Result<Vec<String>, sqlx::Error> {
        let mut res = Vec::new();
        for row in vec {
            res.push(row.content);
        }
        return Ok(res);
    }
}

#[derive(Serialize, Deserialize)]
pub struct Text {
    pub id: String,
    pub text: String,
}

#[derive(Serialize, Deserialize)]
pub struct Title {
    pub id: String,
    pub title: String,
}

fn from_sqlx(err: sqlx::Error) -> Error {
    match err {
        _not_found => Error::BadClientData,
        _ => Error::InternalError,
    }
}

pub fn from_option(option: Option<sqlx::Error>) -> Option<Error> {
    match option {
        Some(err) => Some(from_sqlx(err)),
        _ => None,
    }
}

pub fn from_result<T>(result: Result<T, sqlx::Error>) -> Result<T, Error> {
    match result {
        Ok(ok) => Ok(ok),
        Err(err) => Err(from_sqlx(err)),
    }
}

pub fn to_json(err: Error) -> Json<Value> {
    match err {
        internal_error => Json(json!({ "err": "internal error" })),
        bad_client_data => Json(json!({ "err": "bad client data" })),
    }
}

pub fn result_to_json(result: Result<String, Error>, param: &str) -> Json<Value> {
    match result {
        Ok(value) => Json(json!({ param: value })),
        Err(err) => to_json(err),
    }
}
