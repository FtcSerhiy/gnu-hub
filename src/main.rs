pub mod core;
pub mod handler;
pub mod repository;
pub mod transport;

use self::handler::init_routes;
use std::net::SocketAddr;

#[tokio::main]
async fn main() {
    let addr = SocketAddr::from(([127, 0, 0, 1], 8080));
    axum::Server::bind(&addr)
        .serve(init_routes().await.into_make_service())
        .await
        .expect("failed to strart http server");
}
