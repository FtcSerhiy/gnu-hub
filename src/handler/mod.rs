pub mod articles;
pub mod comments;

use self::{articles::*, comments::*};
use crate::{core::traits::Repositories, repository::Repository, transport};
use axum::{
    extract::Extension,
    routing::{delete, get, post, put},
    Router,
};
use std::sync::Arc;

pub async fn init_routes() -> Router {
    let transport = Arc::new(transport::new::<Repository>(Repository::new().await));
    Router::new()
        .nest(
            "/articles",
            Router::new()
                .route("/create-article", post(create_article))
                .route("/get-article-id", get(get_article_id))
                .route("/update-article-text", put(update_article_text))
                .route("/update-article-title", put(update_article_title))
                .route("/:id/delete-article", delete(delete_article)),
        )
        .nest(
            "/:title",
            Router::new()
                .route("/add-comment", post(add_comment))
                .route("/get-all-comments", get(get_all_comments)),
        )
        .layer(Extension(transport))
}
