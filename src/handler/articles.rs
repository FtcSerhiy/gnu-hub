use crate::core::{models::*, traits::transport::Articles};
use crate::repository::Repository;
use crate::transport::Transport;
use axum::{
    extract::{Extension, Path},
    Json,
};
use serde_json::{json, Value};
use std::sync::Arc;

pub async fn create_article(
    Extension(transport): Extension<Arc<Transport<Repository>>>,
    Json(article): Json<Article>,
) -> Json<Value> {
    result_to_json(transport.create_article(article).await, "id")
}

pub async fn get_article_by_title(
    Extension(transport): Extension<Arc<Transport<Repository>>>,
    Path(title): Path<String>,
) -> Json<Value> {
    result_to_json(transport.get_article_by_title(title).await, "text")
}

pub async fn get_article_id(
    Extension(transport): Extension<Arc<Transport<Repository>>>,
    Json(auth): Json<Auth>,
) -> Json<Value> {
    result_to_json(
        transport.get_article_id(auth.name, auth.password).await,
        "id",
    )
}

pub async fn update_article_text(
    Extension(transport): Extension<Arc<Transport<Repository>>>,
    Json(text): Json<Text>,
) -> Json<Option<Error>> {
    Json(transport.update_article_text(text.id, text.text).await)
}

pub async fn update_article_title(
    Extension(transport): Extension<Arc<Transport<Repository>>>,
    Json(title): Json<Title>,
) -> Json<Value> {
    match transport.update_article_title(title.id, title.title).await {
        Some(err) => to_json(err),
        None => Json(json!({"none": "ok"})),
    }
}

pub async fn delete_article(
    Extension(transport): Extension<Arc<Transport<Repository>>>,
    Path(id): Path<String>,
) {
    transport.delete_article(id);
}
