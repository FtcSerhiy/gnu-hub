use crate::{
    core::{models::*, traits::transport::Comments},
    repository::Repository,
    transport::Transport,
};
use axum::{
    extract::{Extension, Path},
    Json,
};
use serde_json::{json, Value};
use std::sync::Arc;

pub async fn add_comment(
    Extension(transport): Extension<Arc<Transport<Repository>>>,
    Json(comment): Json<Comment>,
) {
    transport.add_comment(comment.title, comment.content).await;
}

pub async fn get_all_comments(
    Extension(transport): Extension<Arc<Transport<Repository>>>,
    Path(title): Path<String>,
) -> Json<Value> {
    match transport.get_all_comments(title).await {
        Ok(vec) => Json(json!({ "comments": vec })),
        Err(err) => to_json(err),
    }
}
