use crate::{
    core::{
        models::*,
        traits::{repository::Comments, Repositories},
    },
    repository::Repository,
};

#[tokio::test]
async fn test_comment() {
    let repository = Repository::new().await;
    match repository.get_all_comments("rust-lang".to_string()).await {
        Ok(vec) => println!("Ok"),
        Err(err) => println!("{err}"),
    }
}
