use crate::core::traits::repository::Comments;
use crate::core::models::Str;
use axum::async_trait;
use sqlx::*;

#[async_trait]
impl Comments for super::Repository {
    async fn add_comment(&self, title: String, comment: String) -> Option<Error> {
        if let Err(err) = query("insert into comments (article_title, content) values ($1, $2)")
            .bind(title)
            .bind(comment)
            .fetch_all(&self.pool)
            .await
        {
            return Some(err);
        }
        return None;
    }
    async fn get_all_comments(&self, title: String) -> Result<Vec<String>> {
        Str::map(query_as!(Str, "select content from comments where article_title = $1", title)
            .fetch_all(&self.pool)
            .await)
    }
}

