pub mod articles;
pub mod comments;
#[cfg(test)]
mod tests;

use crate::core::traits::Repositories;
use axum::async_trait;
use dotenv::dotenv;
use sqlx::postgres::{PgPool, PgPoolOptions};

pub struct Repository {
    pool: PgPool,
}

#[async_trait]
impl Repositories for Repository {
    async fn new() -> Repository {
        dotenv().ok();

        Repository {
            pool: PgPoolOptions::new()
                .connect(&std::env::var("DATABASE_URL").unwrap())
                .await
                .unwrap(),
        }
    }
}
