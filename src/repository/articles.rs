use crate::core::{models::{Str, Article}, traits::repository::Articles};
use axum::async_trait;
use sqlx::*;

#[async_trait]
impl Articles for super::Repository {
    async fn create_article(&self, article: Article) -> Result<String> {
        let id: (i32,) = query_as(
            "insert into articles (title, content, password) values ($1, $2, $3) returning id",
        )
        .bind(article.title)
        .bind(article.content)
        .bind(article.password)
        .fetch_one(&self.pool)
        .await?;
        return Ok(id.0.to_string());
    }
    async fn get_article_id(&self, title: String, password: String) -> Result<String> {
        let id: (i32,) = query_as("select id from articles where title = $1 and password = $2")
            .bind(title)
            .bind(password)
            .fetch_one(&self.pool)
            .await?;
        return Ok(id.0.to_string());
    }
    async fn get_article_by_title(&self, title: String) -> Result<String> {
        let text = query_as!(Str, "select content from articles where title = $1", title)
            .fetch_one(&self.pool)
            .await?;
        return Ok(text.content);
    }
    async fn update_article_text(&self, id: String, text: String) -> Option<Error> {
        if let Err(err) = query("update articles set text = $1 where id = $2")
            .bind(text)
            .bind(id)
            .fetch_one(&self.pool)
            .await
        {
            return Some(err);
        }
        return None;
    }
    async fn update_article_title(&self, id: String, title: String) -> Option<Error> {
        if let Err(err) = query("update articles set title = $1 where id = $2")
            .bind(title)
            .bind(id)
            .fetch_one(&self.pool)
            .await
        {
            return Some(err);
        }
        return None;
    }
    async fn delete_article(&self, id: String) {
        query("delete from articles where id = $1")
            .bind(id.parse::<i32>().unwrap())
            .fetch_one(&self.pool)
            .await;
    }
}
