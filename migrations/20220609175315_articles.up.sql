create table if not exists articles
(
	id serial primary key,
	title varchar(50) not null unique,
	content text not null,
	password varchar(10) not null
);

create table comments
(
	id serial primary key,
	content text not null,
	article_title varchar(50) references articles(title) not null
);
